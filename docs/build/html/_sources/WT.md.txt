# Werkstofftechnik
                            
<center>

![Bild-Titel](https://www.fachhochschule.de/Pic/Content/technik_werkstoff_DE_BOTTOM/Materialwissenschaften%20und%20Werkstofftechnik%20Fotolia_27457771_S.jpg?1560251579)                    
                        
</center>
                        
---
                        
## 1 Werkstoffstruktur: Die innere Ordnung der Werkstoffe

### 1.1 Lernziele zu Kapitel 1

• Sie erklären die wichtigsten Kristallstrukturen der Metalle und die daraus resultierenden spezifischen Eigenschaften.

• Sie erläutern die verschiedenen Arten von Gitterfehlern und ihre Bedeutung für Metalle.

• Sie formulieren situativ Aussagen zu werkstofftechnischen Themen trennscharf und
verwenden dabei die korrekten Fachbegriffe.

• Sie reflektieren selbständig über wesentliche und unwesentliche Aspekte bei
werkstofftechnischen Fragestellungen (Übungsfragen, ...).

--


### 1.2 Der Aufbau von Werkstoffen im perfekten Gitter

Der strukturelle Aufbau der Werkstoffe bestimmt die Werkstoffeigenschaften und damit
die Anwendungsgebiete. Das betrifft alle strukturellen Aspekte vom „Kleinsten“ (Bindungen,
Atomanordnung, Gefüge, …) bis zum „Größten“ (Bauteile: Geometrie, Oberflächenqualität, …).
Nach der Entdeckung der Röntgenstrahlung (X-Strahlen, X-Rays) durch Wilhelm Conrad
Röntgen im Jahr 1895 wurden etwa ab dem Beginn der 20. Jahrhunderts Kristalle mit
Röntgenstrahlen untersucht. Ziel war die Gewinnung von Erkenntnissen zu Gitteraufbau und
Kristallstruktur (Röntgenfeinstrukturanalyse).

<center>
                        
![Bild-Titel](‪C:\Users\NK\Desktop\WT\1.jpg) 
                        
</center>

--

Amorphe Strukturen zeigen z. B. Kunststoffe und Glas. Es gibt nur Nahordnung (keine
Fernordnung). Die Atome sind regellos angeordnet. Es besteht kein Gitter!
Zur Vereinfachung betrachten wir zunächst ideale Kristalle mit einer mathematisch
beschreibbaren perfekten Gitterstruktur (siehe Abb. 1.2.1 Mitte und rechts):

• Kristalline Strukturen gibt es z. B. bei Metallen und Diamant. Hier liegt Nahordnung und
Fernordnung vor. Die Atome sind 3-dimensional periodisch angeordnet
-> Kristallgitter!

• Die Elementarzelle ist die kleinste Baueinheit eines Kristallgitters. Sie enthält alle Informationen zum Aufbau des Raumgitters (Gitterparameter a, b, c und Achsenwinkel α, β, γ).

• Das Raumgitter entsteht durch 3-dimensional periodisches Aneinanderreihen von
Elementarzellen.

• Die Kristallgittertypen der wirtschaftlich wichtigsten Metalle sind kfz, krz und hdp.
In diesem Kapitel erfahren wir schon ansatzweise etwas über die plastische (bleibende)
Verformung von Metallen. Später werden wir diesen wichtigen Punkt weiter vertiefen.

--

Metalle haben im „Normalfall“ im festen Zustand nur eine Gitterstruktur, die sich nicht ändert.
So hat Magnesium im festen Zustand immer eine hdp-Struktur, unabhängig von der Temperatur.
Manche Metalle verhalten sich jedoch polymorph. Diese Metalle weisen bei verschiedenen
Temperaturen verschiedene Gitterstrukturen auf. Dies spielt in der Praxis eine wichtige Rolle
bei Metallen wie Eisen, z. B. die Umwandlung krz ↔ kfz. Bei dieser Änderung der Gitterstruktur
ändert sich auch die Packungsdichte (Volumenänderung!). Das ist entscheidend für viele
Prozesse wie z. B. die Wärmebehandlung von Stahl. Stahl ist eine Legierung aus Eisen (Fe)
und Kohlenstoff (C). Auch Ti, Co und Zr sind polymorph.

Wichtige Kennzeichen der Kristallgittertypen sind z. B. die Zahl ihrer Gleitsysteme (ein GS =
Kombination aus Gleitebene und zugehörige Gleitrichtung) und ihre Packungsdichte (PD).
Eine hohe Packungsdichte bedeutet geringe Kräfte zur plastischen Verformung (und
umgekehrt). Deshalb sind besonders dicht mit Atomen besetzte Ebenen Gleitebenen
(bevorzugte Ebenen der plastischen Verformung, energetisch günstig zum Gleiten).

Gleitebenen bewegen sich energetisch günstig in bevorzugten Gleitrichtungen durch „Täler“
in den Gleitebenen (= „Täler“ zwischen benachbarten Atomreihen) Das sind Richtungen mit
größter Packungsdichte der Atome.
Viele Gleitsysteme bedeuten viele Gleitmöglichkeiten und damit eine gute plastische
Verformbarkeit (und umgekehrt). Genaueres zur plastischen Verformung folgt später.



--

### 1.3 Richtungsabhängigkeit von Werkstoffeigenschaften

Elementarzellen (und deshalb auch Gitter) zeigen in Abhängigkeit von der betrachteten
Richtung verschiedene Abstände zwischen den Atomen und somit richtungsabhängige
Eigenschaften. Je geringer die Atomabstände in einer Richtung sind, desto größer ist z. B. der
E-Modul für diese Richtung und umgekehrt. Auch andere mechanische Eigenschaften wie die
Dehn- / Streckgrenze, die Zugfestigkeit und die plastische Verformbarkeit (Bruchdehnung)
hängen von der betrachteten Richtung im Kristall ab.

--

Erläuterungen zur Richtungsabhängigkeit von Werkstoffeigenschaften:

Quasiisotroper Polykristall (Vielkristall):

o Jeder Kristall hat für sich betrachtet eine Vorzugsrichtung (Pfeil).

o Die Vorzugsrichtungen sind jedoch statistisch verteilt.

o Nach außen hin gibt es deshalb keine Vorzugrichtung (Kristall nach außen isotrop).

Anisotroper Polykristall (Vielkristall mit Textur):

o Jeder Kristall hat für sich betrachtet eine Vorzugsrichtung (Pfeil).

o Die Vorzugsrichtungen sind alle in eine ähnliche Richtung orientiert.

o Nach außen hin gibt es deshalb eine Vorzugrichtung (Kristall nach außen anisotrop).

Anisotroper Einkristall:

o Der Kristall hat keine Korngrenzen und insgesamt nur eine Vorzugsrichtung (Pfeil).

o Nach außen hin gibt es deshalb eine Vorzugrichtung (Kristall nach außen anisotrop).

--

Texturen entstehen durch plastische Verformung (z. B. Kaltwalzen von Blechen, Ziehen von
Drähten) oder die gerichtete Erstarrung von Schmelzen.
Erwünscht sind Texturen bei kaltgezogenen Drähten, Klaviersaiten, Nägeln (hohe Festigkeit).
Unerwünscht sind Texturen in der Umformtechnik (Tiefziehen von Blechen, …).

---

## 2 Mechanische Eigenschaften von Metallen

### 2.1 Lernziele zu Kapitel 2

• Sie beschreiben das elastische und plastische Verhalten von Metallen und verwenden
dazu die passenden Werkstoffkennwerte.

• Sie erklären, wie sich die unterschiedlichen Gitterfehler auf makroskopischer und
mikroskopischer Ebene auf das Werkstoffverhalten auswirken.

• Sie erläutern wichtige Mechanismen zur Festigkeitssteigerung von Metallen.

• Sie formulieren situativ Aussagen zu werkstofftechnischen Themen trennscharf und
verwenden dabei die korrekten Fachbegriffe.

• Sie reflektieren selbständig über wesentliche und unwesentliche Aspekte bei
werkstofftechnischen Fragestellungen (Übungsfragen, ...).

--

### 2.2 Elastische Verformung (linear elastisch, energieelastisch)
Eine elastische Verformung liegt vor, wenn nach Be- und Entlastung eines Bauteils seine
Atome nur wenig (nicht bleibend) aus ihrer Position ausgelenkt wurden (siehe Abbildung):
1. Startpunkt.
2. Elastische Verformung (Hooke´sche Gerade, unterhalb der Dehn- / Streckgrenze).
3. Endpunkt = Startpunkt 1. Die elastische Dehnung εelastisch geht komplett zurück.

In der Praxis ist die elastische Verformung wichtig für Bauteile wie Zahnräder, Wellen, Federn.

-- 
### 2.3 Plastische Verformung
Eine plastische Verformung liegt vor, wenn nach Be- und Entlastung eines Bauteils seine
Atome bleibend aus ihrer Position ausgelenkt wurden. Nach Entlastung bleibt somit eine
plastische Verformung zurück (siehe Abbildung):
1. Startpunkt.
2. Elastische Verformung (Hooke´sche Gerade, unter der Dehn- / Streckgrenze).
Hinweis: Maximale elastische Verformung für Metalle: nur ca. 0,1 bis 0,2 Prozent!
3. Elastische und plastische Verformung (über der Dehn- / Streckgrenze).
4. Endpunkt. Die elastische Dehnung εelastisch ging zurück und die plastische Dehnung
εplastisch bleibt bestehen.

In der Praxis ist die plastische Verformung wichtig für die Umformtechnik (z. B. Walzen,
Tiefziehen, Gewinderollen) und als Verformungsreserve z. B. bei Wellen und Zahnrädern.

-- 
#### 2.3.1 Die „übliche“ Bewegung von Versetzungen

Eine plastische Verformung (makroskopisch mit dem Auge sichtbar) bedeutet das Wandern
sehr vieler Versetzungen innerhalb der Metalls (also auf der mikroskopischen Ebene im
Werkstoff). Plastische Verformung ist auch über Zwillingsbildung möglich.
Die Versetzungsbewegung benötigt ausreichend hohe Schubspannungen (τ > τkritisch) auf
der Kristallgitterebene. Die Schubspannung τkritisch kann somit als eine Art „Streckgrenze
für die Versetzungsbewegung auf Kristallgitterebene“ aufgefasst werden.
Eine technische Legierung von etwa einem Kubikzentimeter Größe enthält viele tausende bis
hunderttausende Kilometer an Versetzungslinien. Nach einer plastischen Verformung kann die
Versetzungsdichte auch ca. um den Faktor 10 bis 100 oder noch höher sein.
Die Zahl der Versetzungen nimmt also mit zunehmender plastischer Verformung (> Re, Rp 0,2)
immer mehr zu. Die Versetzungen behindern sich beim Wandern zunehmend gegenseitig. Die
weitere plastische Verformung wird immer schwieriger, erfordert also immer höhere Kräfte.
Es kommt zur (Kalt-)Verfestigung durch (Kalt-)Verformung (= Verformungsverfestigung).
Versetzungen haben somit einen „zweischneidigen“ Charakter: Sie ermöglichen die
plastische Verformung. Zu viele Versetzungen erschweren diese jedoch!

--- 

## 3 Thermisch aktivierte Vorgänge
### 3.1 Lernziele zu Kapitel 3

• Sie erklären die <green> Diffusion , wenden einfache </green>Gleichungen zur Diffusion korrekt an und
erläutern Praxisbeispiele.

• Sie beschreiben die Erholung und die Rekristallisation als wichtige Arten der
Wärmebehandlung bei Metallen zur Beseitigung von Gitterfehlern.

• Sie stellen die Verformung von Metallen bei erhöhter Temperatur dar (Kriechen).

• Sie formulieren situativ Aussagen zu werkstofftechnischen Themen trennscharf und
verwenden dabei die korrekten Fachbegriffe.


• Sie reflektieren selbständig über wesentliche und unwesentliche Aspekte bei
werkstofftechnischen Fragestellungen (Übungsfragen, ...).

--

## 3.2 Diffusion
Diffusion bei Metallen ist ein thermisch aktivierter Platzwechsel von Atomen und / oder
Leerstellen im Gitter.
Atome in Metallen <green>schwingen</green> um ihre Ruhelage und stoßen dabei immer wieder mit
benachbarten Atomen zusammen. Dabei kommt es zum Energieaustausch zwischen den
Atomen. Es gibt somit Atome mit hoher und niedriger Energie. Atome mit hoher Energie können
(falls sie zufällig in der richtigen Richtung unterwegs sind) ihren Platz im Gitter verlassen und
diesen mit einer Leerstelle tauschen. Dann hat sich das Atom um einen Platz in die eine
Richtung bewegt und die Leerstelle in die entgegengesetzte Richtung. „Hohe Energie“ des
Atoms bedeutet, dass die Aktivierungsenergie zum Verlassen des Gitterplatzes vorhanden ist.
Die Aktivierungsenergie für die Diffusion kommt also aus der Schwingung der Atome und
solche Prozesse heißen deshalb „thermisch aktiviert“. Weitere Beispiele für thermisch
aktivierte Prozesse sind z. B. die Erholung und Rekristallisation von Metallen.

Damit folgt: Da bei hoher Temperatur viel mehr Schwingungsenergie zur Verfügung steht,
funktioniert Diffusion bei hoher Temperatur viel leichter und schneller als bei niedriger
Temperatur. Zusätzlich gibt es bei hoher Temperatur viel mehr Leerstellen, die für den
Platzwechsel von Atomen genutzt werden können.
Warum findet Diffusion im Werkstoff statt?
Werkstoffe sind bestrebt, einen energetisch günstigen Zustand einzunehmen
(thermodynamischer Gleichgewichtszustand). Steht genügend Energie zur Verfügung und auch
ausreichend Zeit, wird sich der Werkstoff in Richtung des thermodynamischen Gleichgewichts
bewegen. Energetisch günstig ist es in der Regel, wenn Fremdatome möglichst gleichmäßig im Werkstoff verteilt sind (Ausnahme: Entmischungsprozesse, werden nicht vertieft).

--

**Es gibt zwei grundlegende Diffusionsmechanismen:**




<left>
Wanderung über Zwischengitterplätze:
•Einlagerungsatome („kleine Fremdatome“) wandern über Zwischengitterplätze. Diese
Atome sind zwar oft klein (Beispiel C), passen aber oft nicht genau in die Lücken des
Wirtsgitters (Beispiel Fe) und erzeugen somit lokal Spannungen. Auch beim Wandern
müssen sich die Zwischengitteratome zwischen anderen Atomen „durchzwängen“.
</left>


<right>
Wanderung über Leerstellen: <br>
•Bei reinen Metallen wandern die Atome des eigenen Gitters und tauschen ihren
Gitterplatz mit Leerstellen (z. B. Diffusion von Fe in reinem Fe).

•Bei Legierungen wandern die Fremdatome im Substitutionsmischkristall und tauschen
ihren Gitterplatz mit Leerstellen (z. B. Fremddiffusion von Ni in Fe).
</right>

--

## Video zum Skript

<center>

<iframe width="560" height="315" src="https://www.youtube.com/embed/__XjQLGpSfg" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</center>


---

<script data-quiz>
				quiz = {"info": {
								"name":    "Teste dein Wissen!",
								"main":    "Versuchen Sie mindestens 80% der Fragen richtig zu beantworten!",
								"level1":  "Jeopardy Ready",
								"level2":  "Jeopardy Contender",
								"level3":  "Jeopardy Amateur",
								"level4":  "Jeopardy Newb",
								"level5":  "Stay in school, kid..." // no comma here
						},
						"questions": [
								{ // Question 1 - Multiple Choice, Single True Answer
										"q": "Warum steigern punktförmige Gitterfehler bei Metallen deren Festigkeit und Härte?",
										"a": [
												{"option": "ASDSADASD",      "correct": false},
												{"option": "DAWDWADWA",     "correct": true},
												{"option": "ADWSADSS",      "correct": false},
												{"option": "ASDSEEDFAS",     "correct": false} // no comma here
										],
										"correct": "<p><span>Richtig!</span> Deswegen!</p>",
										"incorrect": "<p><span>Falsch.</span> Deswegen</p>" // no comma here
								},
								{ // Question 2 - Multiple Choice, Multiple True Answers, Select Any
	"q": "Which of the following best represents your preferred breakfast?",
	"a": [
		{"option": "Bacon and eggs",               "correct": false},
		{"option": "Fruit, oatmeal, and yogurt",   "correct": true},
		{"option": "Leftover pizza",               "correct": false},
		{"option": "Eggs, fruit, toast, and milk", "correct": true} // no comma here
	],
	"select_any": true,
	"correct": "<p><span>Nice!</span> Your cholestoral level is probably doing alright.</p>",
	"incorrect": "<p><span>Hmmm.</span> You might want to reconsider your options.</p>" // no comma here
},
{ // Question 3 - Multiple Choice, Multiple True Answers, Select All
	"q": "Where are you right now? Select ALL that apply.",
	"a": [
		{"option": "Planet Earth",           "correct": true},
		{"option": "Pluto",                  "correct": false},
		{"option": "At a computing device",  "correct": true},
		{"option": "The Milky Way",          "correct": true} // no comma here
	],
	"correct": "<p><span>Brilliant!</span> You're seriously a genius, (wo)man.</p>",
	"incorrect": "<p><span>Not Quite.</span> You're actually on Planet Earth, in The Milky Way, At a computer. But nice try.</p>" // no comma here
},
{ // Question 4
	"q": "How many inches of rain does Michigan get on average per year?",
	"a": [
		{"option": "149",    "correct": false},
		{"option": "32",     "correct": true},
		{"option": "3",      "correct": false},
		{"option": "1291",   "correct": false} // no comma here
	],
	"correct": "<p><span>Holy bananas!</span> I didn't actually expect you to know that! Correct!</p>",
	"incorrect": "<p><span>Fail.</span> Sorry. You lose. It actually rains approximately 32 inches a year in Michigan.</p>" // no comma here
},
{ // Question 5
	"q": "Is Earth bigger than a basketball?",
	"a": [
		{"option": "Yes",    "correct": true},
		{"option": "No",     "correct": false} // no comma here
	],
	"correct": "<p><span>Good Job!</span> You must be very observant!</p>",
	"incorrect": "<p><span>ERRRR!</span> What planet Earth are <em>you</em> living on?!?</p>" // no comma here
} // no comma here
						]
				
						}
			</script>

